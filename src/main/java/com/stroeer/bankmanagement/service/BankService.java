package com.stroeer.bankmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stroeer.bankmanagement.dao.BankAccountRepository;
import com.stroeer.bankmanagement.dao.UserRepository;
import com.stroeer.bankmanagement.entity.BankAccountEntity;
import com.stroeer.bankmanagement.entity.Roles;
import com.stroeer.bankmanagement.entity.UserEntity;

@Service
public class BankService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    public UserEntity getUser(String firstName, String lastName) {
	return userRepository.findUserByFirstNameAndLastName(firstName, lastName);
    }

    public UserEntity saveUser(String firstName, String lastName, Roles role) {

	String firstNameValidation = "[A-Z]+[a-z]*";
	String lastNameValidation = "[A-Z]+([ '-]?[a-zA-Z]+)*";

	if (firstName.matches(firstNameValidation) && lastName.matches(lastNameValidation)) {
	    UserEntity user = new UserEntity();
	    user.setFirstName(firstName);
	    user.setLastName(lastName);
	    user.setRole(role);
	    return userRepository.save(user);
	} else {
	    System.out.println("The validation of first name and last name failed");
	    return null;
	}

    }

    public BankAccountEntity createBankAccountEntity(UserEntity user, String IBAN, UserEntity currentUser) {
	if (userRepository.findUserByFirstNameAndLastName(user.getFirstName(), user.getLastName()) != null) {

	    List<BankAccountEntity> bankAccounts = bankAccountRepository.findBankAccountByUser(user);
	    BankAccountEntity newBankAccount = new BankAccountEntity();

	    if (bankAccounts.size() > 0) {

		newBankAccount.setIBAN(bankAccounts.get(0).getIBAN());

	    } else {

		String ibanRegex = "^[A-Z]{2}[0-9]{2}(?:[ ]?[0-9]{4}){4}(?:[ ]?[0-9]{1,2})?$";
		if (IBAN.matches(ibanRegex)) {
		    List<BankAccountEntity> bankAccountsWithIban = bankAccountRepository.findBankAccountByIBAN(IBAN);
		    if (bankAccountsWithIban.size() == 0) {
			newBankAccount.setIBAN(IBAN);
		    } else {
			// should throw an exception
			System.out.println("The Iban is provided to a different user");
			return null;
		    }

		} else {
		    // exception thrown don't want to break console appliaction;
		    System.out.println("The IBAN is not a correct IBAN");
		    return null;
		}
	    }

	    newBankAccount.setUser(user);
	    newBankAccount.setAdministratorOpenedAccount(currentUser);
	    return bankAccountRepository.save(newBankAccount);
	} else {
	    // exception should be thrown
	    System.out.println("This user does not exist");
	    return null;
	}
    }

    public List<BankAccountEntity> getBankAccountsOpenedByUser(UserEntity admin) {
	return bankAccountRepository.findBankAccountByAdministratorOpenedAccount(admin);
    }

    public void deleteAccountById(long id, UserEntity currentUser) {
	BankAccountEntity bankAccount = bankAccountRepository.findById(id).orElse(null);
	if (bankAccount == null || !bankAccount.getAdministratorOpenedAccount().equals(currentUser)) {
	    // throw exception in normal case but we don't want to break the console
	    // application
	    System.out.println("you can't delete this bank accound as it does not exist or you did not create it");
	} else {
	    bankAccountRepository.deleteById(id);
	}

    }

    public void deleteAccountByIBAN(String IBAN, UserEntity currentUser) {
	List<BankAccountEntity> bankAccounts = bankAccountRepository
		.findBankAccountByAdministratorOpenedAccountAndIBAN(currentUser, IBAN);
	if (bankAccounts.size() == 0) {
	    // throw exception in normal case but we don't want to break the console
	    // application
	    System.out.println("you can't delete this bank accound as it does not exist or you did not create it");
	} else {

	    bankAccountRepository.deleteByIBANAndAdministratorOpenedAccount(IBAN, currentUser);
	}

    }

    public BankAccountEntity editAccountByID(long id, UserEntity currentUserEntity, String IBAN, String firstName,
	    String lastName) {
	System.out.println("I am not sure what to edit therefore I left it empty");
	return null;
    }
}
