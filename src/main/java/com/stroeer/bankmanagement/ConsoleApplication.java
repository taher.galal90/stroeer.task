package com.stroeer.bankmanagement;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.stroeer.bankmanagement.entity.Roles;
import com.stroeer.bankmanagement.entity.UserEntity;
import com.stroeer.bankmanagement.service.BankService;

@Component
@Profile("!test")
public class ConsoleApplication implements CommandLineRunner {

    @Autowired
    private BankService bankService;

    @Override
    public void run(String... args) throws Exception {
	boolean exit = false;
	Scanner scanner = new Scanner(System.in);
	while (true) {
	    System.out.println("Please select from the following tasks");
	    System.out.println("1. Login");
	    System.out.println("2. Exit");

	    String value = scanner.nextLine();
	    try {
		int input = Integer.parseInt(value);
		if (input != 2 && input != 1) {
		    System.out.println("The value you instearted is a wrong number please try again");
		} else if (input == 1) {
		    System.out.println("Please enter the first name");
		    String firstName = scanner.nextLine();
		    System.out.println("Please enter the last name");
		    String lastName = scanner.nextLine();

		    UserEntity user = bankService.getUser(firstName, lastName);
		    if (user == null) {
			while (user == null) {
			    System.out.println(
				    "the first name and last name you entered are wrong do you want to try again please say no if you want to exit");
			    String response = scanner.nextLine();
			    if ("no".equals(response)) {
				break;
			    } else {
				user = bankService.getUser(firstName, lastName);
				System.out.println("Please enter the first name");
				firstName = scanner.nextLine();
				System.out.println("Please enter the last name");
				lastName = scanner.nextLine();
				user = bankService.getUser(firstName, lastName);
			    }
			}
		    }

		    if (user != null && user.getRole() == Roles.System_Admin) {
			System.out.println("1. Do you want to create a bank account");
			System.out.println("2. Do you want to create a new user");
			System.out.println("3. list all bank accounts");
			System.out.println("4 Delete bank account by IBAN");
			System.out.println("5 Delete bank account by id");
			System.out.println("6 edit bank account by id");
			String response = scanner.nextLine();

			try {
			    int responseNumber = Integer.parseInt(response);
			    if (responseNumber == 1) {
				System.out.println("please enter first name");
				String firstNameOfUser = scanner.nextLine();
				System.out.println("please enter last name of user");
				String lastNameOfUser = scanner.nextLine();
				System.out.println(
					"please enter the IBAN take care if the user already exists the IBAN will be taken");
				String IBAN = scanner.nextLine();
				createBankAccount(firstNameOfUser, lastNameOfUser, IBAN, user);
			    } else if (responseNumber == 2) {
				System.out.println("Please enter new user first name");
				String firstNameOfUser = scanner.nextLine();
				System.out.println("Please enter new user last name");
				String lastNameOfUser = scanner.nextLine();
				System.out.println("Please select role either System_admin || User");
				String roleString = scanner.nextLine();
				Roles role = Roles.valueOf(roleString);
				createUser(firstNameOfUser, lastNameOfUser, role);
			    } else if (responseNumber == 3) {
				bankService.getBankAccountsOpenedByUser(user)
					.forEach(currentUser -> System.out.println(currentUser));

			    } else if (responseNumber == 4) {
				System.out.println("Please enter the IBAN you want to delete");
				String IBAN = scanner.nextLine();
				bankService.deleteAccountByIBAN(IBAN, user);
			    } else if (responseNumber == 5) {
				System.out.println("Please enter the ID of the account to delete");
				String idString = scanner.nextLine();
				long id = Long.parseLong(idString);
				deleteBankAccountById(id, user);

			    } else if (responseNumber == 6) {

			    }

			} catch (NumberFormatException e) {
			    System.out.println("number you entered is wrong you will need to login again sorry");
			}

		    } else {
			System.out.println("current user functionality will come soon please wait .....");
		    }

		} else {
		    break;
		}

	    } catch (NumberFormatException e) {
		System.out.println("The value you insearted is not a number please insert a value again");
	    }

	}

    }

    private void createBankAccount(String firstName, String lastName, String IBAN, UserEntity admin) {
	UserEntity user = bankService.getUser(firstName, lastName);
	if (user == null) {
	    System.out.println("the user you entered does not exist please try again later");
	} else {
	    bankService.createBankAccountEntity(user, IBAN, admin);
	}
    }

    private void deleteBankAccountById(long id, UserEntity currentUser) {
	bankService.deleteAccountById(id, currentUser);
    }

    private void createUser(String firstName, String lastName, Roles role) {
	bankService.saveUser(firstName, lastName, role);
    }

    private void deleteBankAccountByIban(String IBAN, UserEntity user) {
	bankService.deleteAccountByIBAN(IBAN, user);
    }

    private void editBankAccount(UserEntity currentUser, String IBAN, String firstName, String lastName, long id) {
	bankService.editAccountByID(id, currentUser, IBAN, firstName, lastName);
    }

}
