package com.stroeer.bankmanagement.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bank_account")
public class BankAccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private UserEntity user;

    private String IBAN;

    @OneToOne
    private UserEntity administratorOpenedAccount;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public UserEntity getUser() {
	return user;
    }

    public void setUser(UserEntity user) {
	this.user = user;
    }

    public String getIBAN() {
	return IBAN;
    }

    public void setIBAN(String iBAN) {
	IBAN = iBAN;
    }

    public UserEntity getAdministratorOpenedAccount() {
	return administratorOpenedAccount;
    }

    public void setAdministratorOpenedAccount(UserEntity administratorOpenedAccount) {
	this.administratorOpenedAccount = administratorOpenedAccount;
    }

    @Override
    public String toString() {
	return "BankAccountEntity [id=" + id + ", user=" + user + ", IBAN=" + IBAN + ", administratorOpenedAccount="
		+ administratorOpenedAccount + "]";
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	BankAccountEntity other = (BankAccountEntity) obj;
	if (IBAN == null) {
	    if (other.IBAN != null)
		return false;
	} else if (!IBAN.equals(other.IBAN))
	    return false;
	if (administratorOpenedAccount == null) {
	    if (other.administratorOpenedAccount != null)
		return false;
	} else if (!administratorOpenedAccount.equals(other.administratorOpenedAccount))
	    return false;
	if (id != other.id)
	    return false;
	if (user == null) {
	    if (other.user != null)
		return false;
	} else if (!user.equals(other.user))
	    return false;
	return true;
    }

}
