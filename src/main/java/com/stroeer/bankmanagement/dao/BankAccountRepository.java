package com.stroeer.bankmanagement.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.stroeer.bankmanagement.entity.BankAccountEntity;
import com.stroeer.bankmanagement.entity.UserEntity;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccountEntity, Long> {

    List<BankAccountEntity> findBankAccountByUser(UserEntity user);

    List<BankAccountEntity> findBankAccountByAdministratorOpenedAccount(UserEntity user);

    List<BankAccountEntity> findBankAccountByAdministratorOpenedAccountAndIBAN(UserEntity user, String IBAN);

    List<BankAccountEntity> findBankAccountByIBAN(String IBAN);

    @Transactional
    void deleteByIBANAndAdministratorOpenedAccount(String IBAN, UserEntity user);
}
