package com.stroeer.bankmanagement.service;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.stroeer.bankmanagement.dao.BankAccountRepository;
import com.stroeer.bankmanagement.dao.UserRepository;
import com.stroeer.bankmanagement.entity.BankAccountEntity;
import com.stroeer.bankmanagement.entity.Roles;
import com.stroeer.bankmanagement.entity.UserEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BankService service;

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Test
    public void testCreateUser() {

	UserEntity user = service.saveUser("Firstname", "LastName", Roles.System_Admin);
	Assert.assertTrue("the user should be saved and the id should have a value", user.getId() != 0);

    }

    // should be done like this
    // @Test(expected = OurOwnException.Class)
    @Test
    public void testCreateUserValidationFailFirstName() {
	UserEntity user = service.saveUser("Firstname1", "LastName", Roles.System_Admin);
	// Should have been an exception test
	Assert.assertNull("The User should be null", user);
    }

    // should be done like this
    // @Test(expected = OurOwnException.Class)
    @Test
    public void testCreateUserValidationFailLastName() {
	UserEntity user = service.saveUser("Firstname", "LastName1", Roles.System_Admin);
	// Should have been an exception test
	Assert.assertNull("User Should be null", user);
    }

    @Test
    public void createBankAccout() {
	UserEntity user = service.saveUser("Firstname", "Lastname", Roles.User);
	UserEntity admin = service.getUser("Taher", "Galal");
	BankAccountEntity bankAccount = service.createBankAccountEntity(user, "DE89 3704 0044 0532 0130 00", admin);

	Assert.assertTrue("Bank Account should be created", bankAccount.getId() != 0);
    }

    @Test
    public void createBankAccoutWrongIBAN() {
	UserEntity user = service.saveUser("Firstname", "Lastname", Roles.User);
	UserEntity admin = service.getUser("Taher", "Galal");
	BankAccountEntity bankAccount = service.createBankAccountEntity(user, "3704 0044 0532 0130 00", admin);

	Assert.assertNull("Bank Account should not be created", bankAccount);
    }

    @Test
    public void createBankAccountUserDoesNotExist() {
	UserEntity user = new UserEntity();
	user.setFirstName("Firstname");
	user.setLastName("Lastname");
	user.setRole(Roles.User);

	UserEntity admin = service.getUser("Taher", "Galal");

	BankAccountEntity bankAccount = service.createBankAccountEntity(user, "DE89 3704 0044 0532 0130 00", admin);
    }

    @Test
    public void failCreateBankAccountIBANOtherUser() {
	UserEntity user1 = service.saveUser("Firstname", "Lastname", Roles.User);
	UserEntity user2 = service.saveUser("Firstnamee", "Lastnamee", Roles.User);
	UserEntity admin = service.getUser("Taher", "Galal");

	BankAccountEntity bankAccount1 = service.createBankAccountEntity(user1, "DE89 3704 0044 0532 0130 00", admin);
	BankAccountEntity bankAccount2 = service.createBankAccountEntity(user2, "DE89 3704 0044 0532 0130 00", admin);

	Assert.assertNotNull(bankAccount1);
	Assert.assertNull(bankAccount2);

    }

    @Test
    public void createBnakAccountUserWithAlreadyAccount() {
	UserEntity user = service.saveUser("Firstname", "Lastname", Roles.User);
	UserEntity admin = service.getUser("Taher", "Galal");
	BankAccountEntity bankAccount = service.createBankAccountEntity(user, "DE89 3704 0044 0532 0130 00", admin);
	BankAccountEntity bankAccount2 = service.createBankAccountEntity(user, "DE89 3704 0044 0532 0130 01", admin);

	Assert.assertNotNull(bankAccount);
	Assert.assertNotNull(bankAccount2);
	Assert.assertTrue("User should always have same IBAN", bankAccount.getIBAN().equals(bankAccount2.getIBAN()));

    }

    @Test
    public void ListAllAccountsOfUserEmpty() {
	UserEntity user1 = service.saveUser("Firstname", "Lastname", Roles.User);
	UserEntity user2 = service.saveUser("Firstnamee", "Lastnamee", Roles.User);
	UserEntity admin1 = service.saveUser("Admin", "Admin", Roles.System_Admin);
	UserEntity admin2 = service.getUser("Taher", "Galal");

	service.createBankAccountEntity(user1, "DE89 3704 0044 0532 0130 00", admin1);
	service.createBankAccountEntity(user2, "DE89 3704 0044 0532 0130 01", admin1);

	Assert.assertTrue("There should be 0 accounts", service.getBankAccountsOpenedByUser(admin2).size() == 0);

    }

    @Test
    public void ListAllAccountsOfUser() {
	UserEntity user1 = service.saveUser("Firstname", "Lastname", Roles.User);
	UserEntity user2 = service.saveUser("Firstnamee", "Lastnamee", Roles.User);

	UserEntity admin = service.getUser("Taher", "Galal");

	service.createBankAccountEntity(user1, "DE89 3704 0044 0532 0130 00", admin);
	service.createBankAccountEntity(user2, "DE89 3704 0044 0532 0130 01", admin);

	Assert.assertTrue("There should be exactly 2 accounts", service.getBankAccountsOpenedByUser(admin).size() == 2);

    }

    @Test
    public void deleteAccount() {
	UserEntity user1 = service.saveUser("Firstname", "Lastname", Roles.User);
	UserEntity admin = service.getUser("Taher", "Galal");
	service.createBankAccountEntity(user1, "DE89 3704 0044 0532 0130 00", admin);

	Assert.assertTrue("There should be exactly 1 account", bankAccountRepository.count() == 1);

	service.deleteAccountByIBAN("DE89 3704 0044 0532 0130 00", admin);

	Assert.assertTrue("Accounts should be empty ", bankAccountRepository.count() == 0);
    }

    @Test
    public void deleteAccountDoesNotexist() {
	UserEntity user1 = service.saveUser("Firstname", "Lastname", Roles.User);
	UserEntity admin = service.getUser("Taher", "Galal");

	service.deleteAccountByIBAN("DE89 3704 0044 0532 0130 00", admin);

	Assert.assertTrue("Accounts should be empty ", bankAccountRepository.count() == 0);
    }

    @Test
    public void deleteAccountNotCreatedByAdmin() {
	UserEntity user1 = service.saveUser("Firstname", "Lastname", Roles.User);
	UserEntity user2 = service.saveUser("Firstnamee", "Lastnamee", Roles.User);
	UserEntity admin1 = service.saveUser("Admin", "Admin", Roles.System_Admin);
	UserEntity admin2 = service.getUser("Taher", "Galal");

	service.createBankAccountEntity(user1, "DE89 3704 0044 0532 0130 00", admin1);
	service.createBankAccountEntity(user2, "DE89 3704 0044 0532 0130 01", admin1);

	Assert.assertTrue("Accounts should be exactly 2 ", bankAccountRepository.count() == 2);
	service.deleteAccountByIBAN("DE89 3704 0044 0532 0130 00", admin2);
	Assert.assertTrue("Accounts should be exactly 2 ", bankAccountRepository.count() == 2);

    }

    @After
    public void cleanUp() {
	bankAccountRepository.deleteAll();

	List<UserEntity> users = userRepository.findAll();

	users.forEach(user -> {
	    if (!user.getFirstName().equals("Taher") || !user.getFirstName().equals("Iman")) {
		userRepository.delete(user);
	    }
	});

    }
}
